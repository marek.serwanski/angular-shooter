import {Injectable} from "@angular/core";
import {AuthService} from "../security/auth.service";

@Injectable({providedIn: 'root'})
export class Target{


  constructor(private authService: AuthService) {
  }

  private targetURL: string = 'http://localhost:8080/';

  public getTargetURL(){
    return this.targetURL;
  }

  public getSecuredTargetURL(uri: string){
    return this.targetURL + uri + '/' + this.authService.getAuthType().toString();
  }

  public getSimpleTargetURL(uri: string){
    return this.targetURL + uri;
  }

  public setTargetURL(newTargetURL : string) {
    this.targetURL = newTargetURL;
  }
}
