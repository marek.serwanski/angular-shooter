import { Component } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {CountSelectOption, SelectsBuilder, TypeSelectOption} from "./builder/selects.builder";
import {ChartBuilder} from "./builder/chart.builder";
import {BackendResponse} from "./model/backend.response";
import {Target} from "./target";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  //selects data
  counts : CountSelectOption[];
  selectedCount: CountSelectOption;
  types: TypeSelectOption[];
  selectedType: TypeSelectOption;

  //chart data
  options: any;
  xAxisData: Array<string> = [];
  totalShootTimes: Array<number> = [];
  serverTimes: Array<number> = [];
  chartBuilding = false;

  constructor(private httpClient: HttpClient, private selectsBuilder: SelectsBuilder, private chartBuilder: ChartBuilder, public target: Target) {
    this.counts = selectsBuilder.getCounts();
    this.selectedCount = this.counts[1];
    this.types = selectsBuilder.getTypes();
    this.selectedType = this.types[0];
    this.buildChart();
  }

  //TODO: clean this fragment
  shoot(): void {
    this.initChart();
    switch(this.selectedType.requestType) {
      case ('FLUX'):
        this.shootFlux(this.target.getSimpleTargetURL(this.selectedType.uri));
        break;
      case ('GET'):
        for (let i = 1; i <= this.selectedCount.value; i++) {
          this.shootSingleGet(this.target.getSecuredTargetURL(this.selectedType.uri), new HttpParams().set('frontendStartTime', Date.now()));
        }
        break;
      case ('POST'):
        for (let i = 1; i <= this.selectedCount.value; i++) {
          this.shootSinglePost(this.target.getSecuredTargetURL(this.selectedType.uri));
        }
        break;
    }
  }

  private shootSingleGet(targetURL: string, params: HttpParams) {
    this.httpClient.get<BackendResponse>(targetURL, { params: params }).subscribe((response: BackendResponse)=>{
      this.handleResponse(Date.now() - response.frontendStartTime, response.backendTotal, '');},
        error => { this.handleResponse(0,0,'err');})
  }

  private shootSinglePost(targetURL: string) {
    this.httpClient.post<BackendResponse>(targetURL, { frontendStartTime: Date.now() }).subscribe((response: BackendResponse)=>{
        this.handleResponse(Date.now() - response.frontendStartTime, response.backendTotal, '');},
      error => { this.handleResponse(0,0,'err');})
  }

  private shootFlux(targetURL: string) {
    let frontendStartTime = Date.now();
    this.httpClient.post<BackendResponse[]>(targetURL, {frontendStartTime, actionsCount: this.selectedCount.value}).subscribe((response: BackendResponse[])=>{
      this.handleResponse(Date.now() - frontendStartTime, response[0].backendTotal, '');
      for(let i=1 ; i<this.selectedCount.value ; i++) {
        this.handleResponse(0, response[i].backendTotal, '');
      }
        },
      error => { this.handleResponse(0,0,'err');})
  }

  private initChart() {
    this.chartBuilding = true;
    this.xAxisData = [];
    this.totalShootTimes = [];
    this.serverTimes = [];
    this.buildChart();
  }

  private handleResponse(shootTime: number, serverTime: number, xAxis: string) {
    this.totalShootTimes.push(shootTime);
    this.serverTimes.push(serverTime);
    this.xAxisData.push(xAxis);
    if (this.xAxisData.length >= this.selectedCount.value) {
      this.buildChart();
      this.chartBuilding = false;
    }
  }

  private buildChart() {
    this.options = this.chartBuilder.build(this.xAxisData, this.totalShootTimes, this.serverTimes);
  }

  setNewTarget($event: any) {
    this.target.setTargetURL($event.target.value);
  }
}
