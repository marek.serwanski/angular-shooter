import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SelectsBuilder {

  types : TypeSelectOption[] = [
    {requestType: "GET", name: "GET - ping pong", uri: "pingpong"},
    {requestType: "POST", name: "POST - ping pong", uri: "pingpong"},
    {requestType: "GET", name: " GET - read simple data", uri: "db/simple"},
    {requestType: "POST", name: " POST - update simple data", uri: "db/simple"},
    {requestType: "POST", name: " POST - single CRUD action", uri: "db/crud"},
    {requestType: "POST", name: " heavy POST - 100x CRUD action", uri: "db/multiplecrud"},
    {requestType: "FLUX", name: " WebFlux - read list", uri: "flux/read"},
    {requestType: "POST", name: " WebFlux - single CRUD action", uri: "flux/crud"},
    {requestType: "POST", name: " heavy WebFlux - 100x CRUD action", uri: "flux/crud/complex"}

  ];
  counts : CountSelectOption[] = [
    {value: 2, name: "2"},
    {value: 5, name: "5"},
    {value: 10, name: "10"},
    {value: 25, name: "25"},
    {value: 100, name: "100"}
  ]

  public getCounts() {
    return this.counts;
  }
  public getTypes() {
    return this.types;
  }
}
export interface TypeSelectOption {
  requestType: string;
  name: string;
  uri: string;
}

export interface CountSelectOption{
  value: number;
  name: string;
}
