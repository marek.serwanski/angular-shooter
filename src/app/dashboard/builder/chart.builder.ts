import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ChartBuilder {

  shootTimeLegend: string = 'total shoot time';
  serverTimeLegend: string = 'server time';

  public build(xAxisData: Array<string>, totalShootTimes: Array<number>, serverTimes: Array<number>): any {

    return {
      legend: {
        data: [this.shootTimeLegend, this.serverTimeLegend],
        align: 'left',
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: this.shootTimeLegend,
          type: 'bar',
          data: totalShootTimes,
          animationDelay: (idx: number) => idx * 100,
        },
        {
          name: this.serverTimeLegend,
          type: 'bar',
          data: serverTimes,
          animationDelay: (idx: number) => idx * 100 + 100,
        },
      ],
      animationEasing: 'elasticOut',
    };
  }
}
