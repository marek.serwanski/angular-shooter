import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthType} from "./auth.type";

@Injectable({providedIn: 'root'})
export class AuthService {

  private basicAuthTokenKey = 'Authorization';
  private currentAuthType = AuthType.none;

  constructor(private httpClient: HttpClient) {}

  //all credentials are hardcoded in server
  public authenticateBasic(target: string) {
    this.httpClient.post<any>(target, {},{observe: "response"}).subscribe(
      res => {
        this.storeBasicAuthToken(res.headers.get(this.basicAuthTokenKey) || '');
        this.currentAuthType = AuthType.basic;
      },
      (error) => {
        console.log('err', error);
      }
    );
  }

  public authenticateFacebook(target: string) {
    this.currentAuthType = AuthType.facebook;
    // this.httpClient.post<any>(target, {},{observe: "response"}).subscribe(
    //   res => {
    //     console.log('?????');
    //     this.currentAuthType = AuthType.facebook;
    //   },
    //   (error) => {
    //     console.log('err', error);
    //   }
    // );
  }

  public logout(target: string) {
    this.httpClient.post(target, {}).subscribe(
  () => {
        localStorage.removeItem(this.basicAuthTokenKey);
      },
  (error) => {
        console.log(error);
        localStorage.removeItem(this.basicAuthTokenKey);
      }
    );
  }

  public getAuthType() {
    return this.currentAuthType;
  }

  public getAuthToken() {
    return localStorage.getItem(this.basicAuthTokenKey) || '';
  }

  public getAuthTokenKeyName() {
    return this.basicAuthTokenKey;
  }

  private storeBasicAuthToken(token: string) {
    localStorage.setItem(this.basicAuthTokenKey, token);
  }
}
