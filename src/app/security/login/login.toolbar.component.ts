import {Component} from "@angular/core";
import {AuthService} from "../auth.service";
import {AuthType} from "../auth.type";
import {Target} from "../../dashboard/target";

@Component({
  selector: 'app-login-toolbar',
  templateUrl: './login.toolbar.component.html',
  styleUrls: ['./login.toolbar.component.css']
})
export class LoginToolbarComponent {

  constructor(private authService: AuthService, private target: Target) {}

  isAuthenticated() {
    return this.authService.getAuthType() != AuthType.none;
  }

  isBasicAuthenticated() {
    return this.authService.getAuthType() == AuthType.basic;
  }

  basicAuthenticate() {
    this.authService.authenticateBasic(this.target.getTargetURL() + "login");
  }

  facebookAuthenticate() {
    this.authService.authenticateFacebook(this.target.getTargetURL() + "facebooklogin");
  }

  logout() {
    this.authService.logout(this.target.getTargetURL() + "logout");
  }
}
